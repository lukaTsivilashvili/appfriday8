package com.example.appfriday7.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import com.example.appfriday7.R
import com.example.appfriday7.databinding.FragmentPasswordBinding

class PasswordFragment : Fragment() {

    private lateinit var binding: FragmentPasswordBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentPasswordBinding.inflate(layoutInflater, container, false)
        changeColor()
        return binding.root
    }


    private fun changeColor(){
        binding.btn1.setOnClickListener{

            binding.linearPass.children.forEach {

                it.setBackgroundResource(R.drawable.ellipse_44)

            }

        }
    }


}